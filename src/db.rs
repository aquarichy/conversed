use std::cmp::Ordering;
use std::rc::Rc;

use sqlx::Error;
use sqlx::types::chrono::{DateTime,Utc};

use conversed::{ChatLog,ChatMessage};
use crate::Args;

mod mysql;
use self::mysql::ConversedMySqlDb;
mod sqlite;
use self::sqlite::ConversedSqliteDb;

pub struct DbService {
    pub id: i64,
}

pub struct DbChatroom {
    pub id: i64,
    pub chat_name: String,
    pub service_name: String,
}

pub struct DbMessage {
    pub sender: String,
    pub date: DateTime<Utc>,
    pub message: String,
}

#[derive(Debug)]
pub enum LoadError {
    SqlxError(sqlx::Error),
    OtherError(String),
}
impl From<sqlx::Error> for LoadError {
    fn from (e : sqlx::Error) -> LoadError {
        return LoadError::SqlxError (e);
    }
}

pub trait ConversedDbIface {
    fn get_all_chatrooms (&self, query_str : &str) -> Result<Vec<DbChatroom>,sqlx::Error>;
    fn get_chatrooms_for_name (&self, query_str : &str, service_id : i64, chat_name : &String) -> Result<Vec<DbChatroom>,sqlx::Error>;
    fn get_messages_for_chat (&self, query_str : &str, chat_id : i64) -> Result<Vec<DbMessage>,sqlx::Error>;
    fn get_service_rows (&self, query_str : &str, service : &String) -> Result<Vec<DbService>,sqlx::Error>;
    fn add_service (&self, insert_service_str : &str, service : &String) -> Result<i64,sqlx::Error>;
    fn add_chat (&self, insert_chat_str : &str, service_id : i64, chat_name : &str) -> Result<i64,sqlx::Error>;
    fn add_message (&self, insert_message_str : &str, service_id : i64, chat_id : i64, msg : ChatMessage, file_path : &str) -> Result<(),sqlx::Error>;
}

#[derive(Clone)]
pub struct ConversedDb {
    db : Rc<dyn ConversedDbIface>,
}

impl ConversedDb {
    pub async fn connect (args : &Args) -> Result<ConversedDb,sqlx::Error> {
        match args.sqlite {
            true => ConversedSqliteDb::connect (&args.sqlite_path).await,
            false => ConversedMySqlDb::connect ().await
        }
    }

    pub async fn get_chatrooms (&self) -> Result<Vec<DbChatroom>,sqlx::Error> {
        let query_str : &str = "
          SELECT c.id, s.name AS s_name, c.name AS c_name
            FROM chatrooms c
           INNER JOIN services s
              ON c.service_id = s.id
           ORDER BY c_name ASC";

        return self.db.get_all_chatrooms (query_str);
    }

    pub async fn list_chatrooms (&self) -> Result<(),sqlx::Error> {
        let chat_rows : Vec<DbChatroom> = self.get_chatrooms ().await?;

        for chat_row in chat_rows {
            println!("{:5}  {:12} {:60}", chat_row.id, chat_row.service_name, chat_row.chat_name);
        }

        return Ok(());
    }

    pub async fn get_messages_for_chat (&self, chat_id : i64) -> Result<Vec<DbMessage>,sqlx::Error> {
        let query_str = "SELECT sender, dt, message FROM messages WHERE chat_id = ?";

        return self.db.get_messages_for_chat (query_str, chat_id);
    }

    pub async fn list_messages_for_chat (&self, chat_id : i64) -> Result<(),sqlx::Error> {
        for msg in self.get_messages_for_chat (chat_id).await? {
            println!("{}   {:>20}: {}", msg.date, msg.sender, msg.message);
        }

        return Ok(());
    }

    async fn get_service_id (&self, service : &String) -> Result<(i64,bool),LoadError> {
        /* Identify servcie */
        let query_service_str : &str = "
          SELECT s.id
            FROM services s
           WHERE s.name = ?";
        let insert_service_str : &str = "INSERT INTO services (name) VALUES (?)";

        let mut service_rows : Vec<DbService> = self.db.get_service_rows (query_service_str, service)?;
        let mut new_service : bool = false;

        let service_id : i64 = match service_rows.len ().cmp (&1) {
            Ordering::Less    => {
                new_service = true;
                self.db.add_service (insert_service_str, service)?
            },
            Ordering::Equal   => service_rows.pop ().unwrap ().id,
            Ordering::Greater => return Err(LoadError::OtherError(format!("Too many services with the same name '{service}'"))),
        };

        return Ok((service_id, new_service));
    }

    async fn get_chat_id (&self, service : &String, service_id : i64, chat_name : &String) -> Result<(i64,bool),LoadError> {
        /* Identify chat */
        let query_chat_str : &str = "
          SELECT c.id
            FROM chatrooms c
           WHERE c.name = ?
             AND c.service_id = ?";
        let insert_chat_str : &str = "INSERT INTO chatrooms (service_id, name) VALUES (?, ?)";

        let mut chat_rows : Vec<DbChatroom> = self.db.get_chatrooms_for_name (query_chat_str, service_id, chat_name)?;
        let mut new_chat : bool = false;
        let chat_id : i64 = match chat_rows.len ().cmp (&1) {
            Ordering::Less => {
                new_chat = true;
                self.db.add_chat (insert_chat_str, service_id, chat_name)?
            },
            Ordering::Equal => chat_rows.pop ().unwrap ().id,
            Ordering::Greater => return Err(LoadError::OtherError(format!("Too many chatrooms for service '{service}' with chat name '{}'", &chat_name))),
        };

        return Ok ((chat_id, new_chat));
    }

    async fn load_messages (&self, messages : Vec<ChatMessage>, service_id : i64, chat_id : i64, file_path : &String) -> (usize, usize, usize) {
        let insert_message_str : &str = "INSERT INTO messages (service_id, chat_id, sender, dt, message, sourcefile) VALUES (?, ?, ?, ?, ?, ?)";

        /* Load messages */
        let mut num_adds : usize = 0;
        let mut num_dups : usize = 0;
        let mut num_errs : usize = 0;
        for msg in messages {
            match self.db.add_message (insert_message_str, service_id, chat_id, msg, file_path) {
                    Ok(_o) => num_adds += 1,
                    Err(e) => {
                        match e {
                            Error::Database(db_error) => {
                                if db_error.code ().unwrap () == "23000" {
                                    // Duplicate record, we'll just skip it
                                    num_dups +=1 ;
                                } else {
                                    eprintln!("Unexpected error: {db_error}");
                                    num_errs += 1;
                                }
                            },
                            _ => { dbg!(e); num_errs += 1 }
                        };
                    },
                };
        }

        return (num_adds, num_dups, num_errs);
    }

    pub async fn load_log (&self, service: &String, chat_log : ChatLog, file_path : &String) -> Result<DbChatroom,LoadError> {
        let (service_id, new_service) : (i64, bool) = self.get_service_id (service).await?;
        let (chat_id, new_chat) : (i64, bool) = self.get_chat_id (service, service_id, &chat_log.name).await?;
        let (num_adds, num_dups, num_errs) : (usize, usize, usize) = self.load_messages (chat_log.messages, service_id, chat_id, file_path).await;

        println!("Chat log loaded. {num_adds:5} added, {num_dups:5} dups, {num_errs:5} errs.  Service {:12} for chatroom {:62} {} {}",
                 format!("'{}'", service),
                 format!("'{}'", chat_log.name),
                 if new_service { "new service!" } else { "" },
                 if new_chat { "new_chat!" } else { "" });

        return Ok(DbChatroom { id: chat_id, chat_name: chat_log.name, service_name: service.to_string () });
    }
}
