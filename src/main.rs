use clap::{Parser,Subcommand};
use std::process;

use conversed::parsers::ChatParsers;
use conversed::{ChatLog, ChatParser, Parse};

#[cfg(feature = "gui")]
mod ui;
#[cfg(feature = "db")]
mod db;
#[cfg(feature = "db")]
use db::DbChatroom;
#[cfg(feature = "db")]
use db::ConversedDb;

#[derive(Subcommand,Debug)]
enum Commands {
    /// Parse a ChatLog and print to screen.  Does not load into the database.
    Parse {
        #[arg(help = "Service whose files we are parsing (e.g. 'meta_html' (for FB and IG HTML), 'meta_json' (for FB and IG JSON), 'cicq').  Use 'auto' to try to guess the service based on the filename.")]
        service: String,
        #[arg(help = "Path to chat log file (e.g. message.html for Facebook) or directory (e.g. for centerICQ)")]
        chat_path: String,
    },
    /// Parse a ChatLog and print to screen.  Does not load into the database.  Guesses the service for the chatlog.
    TestParseAuto {
        #[arg(help = "Path to chat log file (e.g. message.html for Facebook) or directory (e.g. for centerICQ)")]
        chat_path: String,
    },
    /// List available services in the terminal
    ListServices {
    },
    #[cfg(feature = "db")]
    /// List chats available in the Conversed DB in the terminal
    ListChats {
    },
    #[cfg(feature = "db")]
    /// List messages for a given chat's ID in the terminal
    ListMessagesForChat {
        #[arg(help = "The numeric ID for a given chat, to show messages for")]
        chat_id: i64,
    },
    #[cfg(feature = "db")]
    /// Parse and load chat logs into the Conversed DB
    Load {
        #[arg(help = "Service whose files we are parsing and loading (e.g. 'meta_html' (for FB and IG HTML), 'meta_json' (for FB and IG JSON), 'cicq')")]
        service: String,
        #[arg(help = "Path to chat log file (e.g. message.html for Facebook) or directory (e.g. for centerICQ) to load into the DB")]
        chat_path: String,
    },
    #[cfg(feature = "db")]
    /// Parse and load chat logs into the Conversed DB.  Auto-detects which servie
    TestLoadAuto {
        #[arg(help = "Path to chat log file (e.g. message.html for Facebook) or directory (e.g. for centerICQ) to load into the DB")]
        chat_path: String,
    },
    #[cfg(feature = "gui")]
    /// View chats and messages in the database using a GUI
    Viewer {
    },
}

#[derive(Parser,Debug)]
#[command(author = "Richard Schwarting",
          version,
          about, /* see Cargo.toml's description field*/
          long_about = None)]

pub struct Args {
    #[command(subcommand)]
    command: Commands,

    /// Use SQLite backend (at ~/.local/share/conversed/conversed.sqlite)
    /// instead of MySQL (configured by ~/.config/conversed/conversed.conf)
    #[arg(short, long, default_value_t = false)]
    sqlite: bool,

    /// Use SQLite backend on DB stored at SQLITE_PATH.  If one does not exist, it will be created.   Implies -s.
    #[arg(long)]
    sqlite_path: Option<String>,
}

async fn parse_auto (chat_path: &String) -> Result<Vec<ChatLog>,String> {
    let parsers : ChatParsers = ChatParsers::new ();

    return match parsers.service_for_filename (chat_path) {
        Some(service_name) => parse_for_service (&service_name, chat_path).await,
        None => Err(format!("Unable to identify a parser for chat log at '{chat_path}'")),
    }
}

async fn parse_for_service (service : &String, chat_path: &String) -> Result<Vec<ChatLog>,String> {
    let parsers : ChatParsers = ChatParsers::new ();
    let parser : ChatParser = parsers.from_service (&service)?;
    let chat_logs : Vec<ChatLog> = parser.parse (&chat_path)?;

    for c in &chat_logs {
        c.print_chat_log ();
    }

    Ok (chat_logs)
}

#[cfg(feature = "db")]
async fn load_auto (chat_path: &String, db : &ConversedDb) -> Result<Vec<DbChatroom>,String> {
    let parsers : ChatParsers = ChatParsers::new ();

    return match parsers.service_for_filename (chat_path) {
        Some(service_name) => load_for_service (&service_name, chat_path, db).await,
        None => Err(format!("Unable to identify a parser for chat log at '{chat_path}'")),
    }
}

#[cfg(feature = "db")]
async fn load_for_service (service : &String, chat_path : &String, db : &ConversedDb) -> Result<Vec<DbChatroom>,String> {
    let parsers : ChatParsers = ChatParsers::new ();
    let parser : ChatParser = parsers.from_service (&service)?;
    let mut chats : Vec<DbChatroom> = vec!();

    let chat_logs : Vec<ChatLog> = parser.parse (&chat_path)?;

    for c in chat_logs {
        let chat : DbChatroom = db.load_log (&service, c, &chat_path).await.expect ("Error in load_log"); /* TODO: expand handling */
        chats.push (chat);
    }

    Ok(chats)
}

fn connect_error (e : sqlx::Error) -> Option<String> {
    /* sqlx_error_to_option */
    Some(format!("Failed to connect.  Error: {e}"))
}

#[async_std::main]
async fn main() {
    let mut args : Args = Args::parse ();

    match args.sqlite_path {
        Some(_) => { args.sqlite = true },
        None => {},
    }

    let err : Option<String> = match &args.command {
        Commands::TestParseAuto { chat_path } => {
            parse_auto (chat_path).await.err ()
        },
        Commands::Parse { service, chat_path } => {
            parse_for_service (service, chat_path).await.err ()
       },
        Commands::ListServices { } => {
            let parsers : ChatParsers = ChatParsers::new ();
            parsers.list_services ();
            None
        },
        #[cfg(feature = "db")]
        Commands::ListChats { } => {
            match ConversedDb::connect (&args).await {
                Ok(db) => db.list_chatrooms ().await.or_else (|e| Err(format!("{e}"))).err (), /* sqlx::Error > Err<String> */
                Err(e) => connect_error (e),
            }
        },
        #[cfg(feature = "db")]
        Commands::ListMessagesForChat { chat_id } => {
            match ConversedDb::connect (&args).await {
                Ok(db) => db.list_messages_for_chat (*chat_id).await.or_else (|e| Err(format!("{e}"))).err (),
                Err(e) => connect_error (e),
            }
        },
        #[cfg(feature = "db")]
        Commands::TestLoadAuto { chat_path } => {
            match ConversedDb::connect (&args).await {
                Ok(db) => load_auto (chat_path, &db).await.err (),
                Err(e) => connect_error (e),
            }
        },
        #[cfg(feature = "db")]
        Commands::Load { service, chat_path } => {
            match ConversedDb::connect (&args).await {
                Ok(db) => load_for_service (service, chat_path, &db).await.err (),
                Err(e) => connect_error (e),
            }
        },
        #[cfg(feature = "gui")]
        Commands::Viewer { } => {
            match ConversedDb::connect (&args).await {
                Ok(db) => { ui::run (db); None }
                Err(e) => connect_error (e),
            }
        },
    };

    match err {
        Some(e) => {
            eprintln!("{e}");
            process::exit (1);
        },
        None => process::exit (0),
    }
}
