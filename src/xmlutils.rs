use roxmltree;
use roxmltree::Node;

pub type PathPart<'a,'b> = (&'a str,&'b str);

pub fn node_get_desc_from_path<'a,'b> (node : &Node<'a,'b>, path : &Vec<PathPart>) -> Option<Node<'a,'b>> {
    let mut cur : Node = *node;

    for part in path {
        cur = match node_get_child_with (&cur, part) {
            Some(n) => n,
            None => return None
        };
    };

    return Some(cur);
}

pub fn node_get_child_with<'a,'b> (node : &Node<'a,'b>, path_part: &PathPart) -> Option<Node<'a,'b>> {
    let node_name : &str = path_part.0;
    let class_name : &str = path_part.1;

    for child in node.children () {
        if child.tag_name ().name () == node_name && (class_name == "*" || child.attribute ("class").unwrap_or ("") == class_name) {
            return Some (child);
        }
    }

    return None;
}

pub fn node_get_text_from_path<'a> (node : &Node<'a, '_>, path : &Vec<PathPart>) -> &'a str {
    return match node_get_desc_from_path (node, path) {
        Some(n) => n.text ().unwrap_or (""),
        None => "",
    };
}

pub fn node_inner_text (node : Node) -> String {
    let mut str : String = String::new();

    for child in node.children () {
        if child.is_text () {
            str.push_str ("  [");
            str.push_str (child.text ().unwrap_or (""));
            str.push_str ("]\n");
        } else {
            str.push_str (node_inner_text (child).as_str ());
        }
    }

    return str;
}

pub fn node_dbg_child_list (parent : Node) -> String {
    let mut children : String = String::new ();
    for child in parent.children () {
        if child.is_element () {
            children.push_str (format!("{}.{}, ", child.tag_name ().name (), child.attribute ("class").unwrap_or ("<NOCLASS>")).as_str ());
        }
    }
    return children;
}

pub fn node_find_descs_with<'a,'b> (node : &Node<'a,'b>, tag_name : &str, class_name : &str) -> Vec<Node<'a,'b>> {
    let mut descs : Vec<Node> = vec!();

    for child in node.children () {
        if child.tag_name ().name () == tag_name && (class_name == "*" || child.attribute ("class").unwrap_or ("") == class_name) {
            descs.push (child);
        } else {
            descs.extend (node_find_descs_with (&child, tag_name, class_name));
        }
    }

    return descs;
}

#[allow(dead_code)]
pub fn node_find_desc_with<'a,'b> (node : &Node<'a,'b>, tag_name : &str, class_name : &str) -> Option<Node<'a,'b>> {
    for child in node.children () {
        if child.tag_name ().name () == tag_name && (class_name == "*" || child.attribute ("class").unwrap_or ("") == class_name) {
            return Some(child);
        } else {
            match node_find_desc_with (&child, tag_name, class_name) {
                Some(o) => return Some(o),
                None => {},
            }
        }
    }

    return None;
}

