#![allow(dead_code)]

use crate::{report_err, ChatLog, ChatMessage, ChatParser, Parse, Person};
use chrono::{DateTime, Utc};
use serde::Deserialize;
use std::fs;
use std::collections::{HashMap, HashSet};
use quick_xml::de;
use regex::Regex;

/* smil:
<smil>
  <head>
    <layout>
      <root-layout width="INT" height="INT" />
      <region id="Image" width="INT" height="INT" left="INT" top="INT" fit="meet" />
      <region id="Text" width="INT" height="INT" left="INT" top="INT" fit="scroll" />
    </layout>
  </head>
  <body>
    <par dur="5000ms">
      <img src="image.jpg" region="Image" begin="0ms" end="5000ms" />
    </par>
  </body>
</smil>
*/

#[derive(Deserialize,Debug)]
struct MmsPartElement {
    #[serde(rename = "@cd")]
    cd: String,              /* "null" or "attachment" */
    #[serde(rename = "@chset")]
    chset: String,           /* "null", "3" or "106" */
    #[serde(rename = "@cid")]
    cid: String,             /* "null" */
    #[serde(rename = "@cl")]
    cl: String,              /* filename, e.g. "video3gpp_2.3gp", "text_1.txt" */
    #[serde(rename = "@ct")]
    ct: String,              /* content type, e.g. "video/mpg4", "image/jpeg", "text/plain", etc. */
    #[serde(rename = "@ctt_s")]
    ctt_s: String,           /* "null" */
    #[serde(rename = "@ctt_t")]
    ctt_t: String,           /* "null" */
    #[serde(rename = "@data")]
    data: Option<String>,   /* base64 encoded content, e.g. of an image; if not present, then dataref="" may exist pointing to a path on the local file system storing said data */
    #[serde(rename = "@dataref")]
    dataref: Option<String>, /* my custom tag */
    #[serde(rename = "@fn")]
    r#fn: String,            /* "null" */
    #[serde(rename = "@text")]
    text: String,            /* may contain SMIL content or a simple string */
    #[serde(rename = "@name")]
    name: String,
    #[serde(rename = "@seq")]
    seq: i8,                 /* 0 or -1 */
}

#[derive(Deserialize,Debug)]
struct MmsPartsElement {
    part: Option<Vec<MmsPartElement>>,
}

#[derive(Deserialize,Debug)]
struct MmsAddrElement {
    #[serde(rename = "@address")]
    address: String, /* phone number */
    #[serde(rename = "@charset")]
    charset: u8,     /* e.g. "106" */
    #[serde(rename = "@type")]
    r#type: u8,      /* e.g. 137, 151 */
}

#[derive(Deserialize,Debug)]
struct MmsAddrsElement {
    addr: Option<Vec<MmsAddrElement>>,
}

#[derive(Deserialize,Debug)]
struct MmsElement {
    parts: MmsPartsElement,
    addrs: MmsAddrsElement,

    #[serde(rename = "@address")]
    address: String,       /* phone number, e.g. "+12225557777" */
    #[serde(rename = "@contact_name")]
    contact_name: String,
    #[serde(rename = "@creator")]
    creator: String,       /* e.g. "com.google.android.apps.messaging", "com.riteshsahu.SMSBackupRestore" */
    #[serde(rename = "@ct_cls")]
    ct_cls: String,        /* "null" */
    #[serde(rename = "@ct_l")]
    ct_l: String,          /* URI, like "http://mms.cherrytelecom.fake:9099/m0-12345678" */
    #[serde(rename = "@ct_t")]
    ct_t: String,          /* content type, application/vnd.wap.multipart.mixed", "application/vnd.wap.multipart.related" */
    #[serde(rename = "@date")]
    date: i64,             /* unix time */
    #[serde(rename = "@date_sent")]
    date_sent: i64,        /* unix time */
    #[serde(rename = "@d_rpt")]
    d_rpt: String,         /* "null", "128", "129" */
    #[serde(rename = "@d_tm")]
    d_tm: String,          /* "null" */
    #[serde(rename = "@exp")]
    exp: String,              /* "null" or e.g. "5000", ... expiry time, in ... ms? s? */
    #[serde(rename = "@locked")]
    locked: i8,            /* 0 */
    #[serde(rename = "@m_cls")]
    m_cls: String,         /* "personal" or "null" */
    #[serde(rename = "@m_id")]
    m_id: String,          /* "msg-id-<NUMBER>" */
    #[serde(rename = "@msg_box")]
    msg_box: i8,           /* 1 or 2 */
    #[serde(rename = "@m_size")]
    m_size: String,           /* "null" or "12345" (size in bytes) */
    #[serde(rename = "@m_type")]
    m_type: String,        /* "128" or "132" */
    #[serde(rename = "@pri")]
    pri: String,           /* "null" or "128" */
    #[serde(rename = "@read")]
    read: i8,              /* 1 */
    #[serde(rename = "@readable_date")]
    readable_date: String, /* localized local date string */
    #[serde(rename = "@read_status")]
    read_status: String,   /* "null" */
    #[serde(rename = "@resp_st")]
    resp_st: String,       /* "null" or "128" */
    #[serde(rename = "@resp_txt")]
    resp_txt: String,      /* "null" */
    #[serde(rename = "@retr_st")]
    retr_st: String,       /* "null" or "128" */
    #[serde(rename = "@retr_txt_cs")]
    retr_txt_cs:  String,  /* "null" or "106" */
    #[serde(rename = "@retr_txt")]
    retr_txt:  String,     /* "null" or "1000:OK" */
    #[serde(rename = "@rpt_a")]
    rpt_a: String,         /* "null" */
    #[serde(rename = "@rr")]
    rr: String,            /* "null", "128" or "129" */
    #[serde(rename = "@seen")]
    seen: i8,              /* 1 */
    #[serde(rename = "@st")]
    st: String,            /* "null" */
    #[serde(rename = "@sub_cs")]
    sub_cs: String,        /* "null" or "106" */
    #[serde(rename = "@sub_id")]
    sub_id: i8,            /* -1, 1, 2 */
    #[serde(rename = "@sub")]
    sub: String,           /* media filename, or "A Picture/Video Message!" */
    #[serde(rename = "@text_only")]
    text_only: i8,         /* 0 or 1 */
    #[serde(rename = "@tr_id")]
    tr_id: String,
    #[serde(rename = "@v")]
    v: i8,                 /* 16 or 18 */
}

#[derive(Deserialize,Debug)]
struct SmsElement {
    #[serde(rename = "@protocol")]
    protocol: i8,

    #[serde(rename = "@type")]
    r#type: i8,               /* 1: recevied (from them) or 2: sent (by you)? */

    /* NOTE: contact information is for messaging partner, not the sender; use '@type' to determine sender */
    #[serde(rename = "@address")]
    address: String,          /* "+12225558888" */
    #[serde(rename = "@contact_name")]
    contact_name: String,     /* absent for <1% of messages TODO why */

    #[serde(rename = "@date")]
    date: i64,                /* unix time, ms; presumably received */
    #[serde(rename = "@date_sent")]
    date_sent: i64,           /* unix time, ms */
    #[serde(rename = "@readable_date")]
    readable_date: String,    /* localized local date */

    #[serde(rename = "@body")]
    body: String,             /* Actual message content */

    #[serde(rename = "@subject")]
    subject: String,          /* "null" */
    #[serde(rename = "@toa")]
    toa: String,              /* "null" */
    #[serde(rename = "@sc_toa")]
    sc_toa: String,           /* "null" */
    #[serde(rename = "@service_center")]
    service_center: String,   /* "+12225559000 */
    #[serde(rename = "@read")]
    read: i8,                 /* 1 (presumably for read, and presumably 0 for unread) */
    #[serde(rename = "@status")]
    status: i8,               /* -1 */
    #[serde(rename = "@locked")]
    locked: i8,               /* 0 */
}

trait MessageAccess {
    fn body (&self) -> &String;
    fn address (&self) -> &String;
    fn contact_name (&self) -> &String;
    fn date (&self) -> i64;
}

impl MessageAccess for SmsElement {
    fn body (&self) -> &String { return &self.body }
    fn address (&self) -> &String { return &self.address }
    fn contact_name (&self) -> &String { return &self.contact_name }
    fn date (&self) -> i64 { return self.date }
}

#[derive(Deserialize,Debug)]
enum SmsOrMms {
    #[serde(rename = "sms")]
    Sms(SmsElement),
    #[serde(rename = "mms")]
    Mms(MmsElement),
}

#[derive(Deserialize,Debug)]
struct SmsesElement {
    #[serde(rename = "@count")]
    count: i64,          /* # of SMS/MMS messages */
    #[serde(rename = "@backup_set")]
    backup_set: String,  /* identifier string */
    #[serde(rename = "@backup_date")]
    backup_date: i64,    /* Unix seconds */

    #[serde(rename = "$value")]
    children: Vec<SmsOrMms>,
}

struct SmsParser {
}

pub fn new () -> ChatParser {
    return ChatParser { service: "sms".to_string (), format: "xml".to_string (), parser: Box::new(SmsParser {})};
}

impl Parse for SmsParser {
    fn can_parse (&self, file: &String) -> bool {
        let re_str : &str = r"(^|/)sms-[12].*\.xml$";
        let re : Regex = match Regex::new (re_str) {
            Ok(re) => re,
            Err(e) => { eprintln!("Error compiling regex: {e}. re_str: '{re_str}'"); return false }
        };

        return re.is_match (file.as_str ());
    }

    fn parse (&self, file: &String) -> Result<Vec<ChatLog>,String> {
        match fs::read_to_string (file) {
            Ok(s) => return self.parse_data (s),
            Err(e) => return report_err (format!("Uh oh, failed to read SMS log file.  Error: {e}")),
        }
    }

    fn parse_data (&self, data : String) -> Result<Vec<ChatLog>,String> {
        let doc : SmsesElement = match de::from_str (data.as_str ()) {
            Ok(d) => d,
            Err(e) => return Err (format!("Error parsing XML data.  Error: {e:?}")),
        };

        let mut chat_map : HashMap<String,(ChatLog,HashMap<String,Person>)> = HashMap::new ();
        let mut chat_logs : Vec<ChatLog> = vec!();

        for src_msg in doc.children {
            match src_msg {
                SmsOrMms::Sms(m) => {
                    let datetime : DateTime<Utc> = match DateTime::from_timestamp (m.date / 1000, (m.date % 1000000000) as u32) {
                        Some(dt) => dt,
                        None => return Err (format!("No date time found for message {m:?}")),
                    };

                    let contact_name : String = if m.contact_name.as_str () == "(Unknown)" {
                        format!("{} ({})", m.contact_name, m.address)
                    } else {
                        m.contact_name
                    };

                    let sender : String = if m.r#type == 2 {
                        String::from ("Me")
                    } else {
                        contact_name.clone ()
                    };

                    let dest_msg : ChatMessage = ChatMessage { sender, datetime, content: m.body, media: vec!() };

                    let (chat_log, people_map) = if chat_map.contains_key (&m.address) {
                        chat_map.get_mut (&m.address).unwrap ()
                    } else {
                        let messages : Vec<ChatMessage> = vec!();
                        let people : HashSet<Person> = HashSet::new ();

                        let new_chat_log : ChatLog = ChatLog { name: contact_name, messages, people };
                        let new_people_map : HashMap<String,Person> = HashMap::new ();

                        chat_map.insert (m.address.clone (), (new_chat_log, new_people_map));

                        chat_map.get_mut (&m.address).unwrap ()
                    };

                    if ! people_map.contains_key::<String> (&m.address) {
                        people_map.insert (m.address.clone (), Person { email: m.address.clone (), name: dest_msg.sender.clone (), username: m.address.clone () });
                    }

                    chat_log.messages.push (dest_msg);
                },
                SmsOrMms::Mms(_m) => {
                    eprintln!("TODO: add support for MMS");
                }
            }
        }

        for (_key, (mut chat_log, mut people_map)) in chat_map.drain () {
            for (_key, person) in people_map.drain () {
                chat_log.people.insert (person);
            }

            chat_logs.push (chat_log);

        }

        return Ok(chat_logs);
    }
}
