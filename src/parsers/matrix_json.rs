use std::fs;
use std::collections::{HashMap, HashSet};
use std::fmt::{Display,Formatter};
use chrono::{DateTime, Utc};
use serde::Deserialize;
use serde_json;
use regex::Regex;

use crate::{ChatParser,Parse,ChatLog,ChatMessage,Person,log};

#[derive(Deserialize,Debug)]
struct MatrixLog {
    messages: Vec<MatrixMessage>,
    room_name: String,
}

#[derive(Deserialize,Debug)]
struct MatrixMessage {
    origin_server_ts: i64,
    sender: String,
    content: MatrixContent,
    r#type: String,
}

#[derive(Deserialize,Debug)]
struct MatrixContent {
    /* for "m.room.message" content */
    msgtype: Option<String>,
    body: Option<String>,

    /* for "m.room.member" content */
    displayname: Option<String>,
}

impl Display for MatrixContent {
    fn fmt (&self, f : &mut Formatter) -> std::fmt::Result {
        match &self.msgtype {
            Some(t) => {
                match t.as_str () {
                    "m.text" | "m.emote" => { /* TODO review m.emote */
                        match &self.body {
                            Some(b) => return write!(f, "{}", b),
                            None => {} /* no body */
                        }
                    },
                    "m.image" | "m.file" | "m.audio" | "m.video" => {
                        match &self.body {
                            Some(b) => return write!(f, "[{}:{}]", t, b),
                            None => {} /* no body */
                        }
                    },
                    _t => {} /* unknown type */
                }
            },
            None => {} /* no type :( */
        }
        return write!(f, "{:?}", &self);
    }
}

struct MatrixJsonParser {
}

pub fn new () -> ChatParser {
    return ChatParser { service: "matrix".to_string (), format: "json".to_string (), parser: Box::new (MatrixJsonParser {}) };
}

impl Parse for MatrixJsonParser {
    fn can_parse (&self, file: &String) -> bool {
        let re_str : &str = r"(^|/)matrix \- .* \- Chat Export - .*\.json$";
        let re : Regex = match Regex::new (re_str) {
            Ok(re) => re,
            Err(e) => { eprintln!("Error compiling regex: {e}. re_str: '{re_str}'"); return false }
        };

        return re.is_match (file);
    }

    fn parse (&self, fpath : &String) -> Result<Vec<ChatLog>,String> {
        let data : String = match fs::read_to_string (fpath) {
            Ok(s) => s,
            Err(e) => {
                let msg : String = format!("Uh oh, couldn't parse chat log: {e}");
                log (&msg);
                return Err(msg);
            }
        };

        return self.parse_data (data);
    }

    fn parse_data (&self, data : String) -> Result<Vec<ChatLog>,String> {
        let log : MatrixLog = match serde_json::from_str (&data) {
            Ok(l) => l,
            Err(e) => {
                let msg : String = format!("Uh oh, couldn't parse chat log: {e}");
                log (&msg);
                return Err(msg);
            }
        };

        let mut messages   : Vec<ChatMessage> = vec!();
        let mut people_map : HashMap<String,String> = HashMap::new ();
        let mut people     : HashSet<Person> = HashSet::new ();

        for matrix_msg in log.messages {
            match matrix_msg.r#type.as_str () {
                "m.room.message" => {
                    let datetime : DateTime<Utc> = DateTime::from_timestamp (matrix_msg.origin_server_ts / 1000,
                                                                             (matrix_msg.origin_server_ts % 1000 * 1000000) as u32)
                        .expect (format!("Invalid timestamp on message: '{:?}'", matrix_msg).as_str ());

                    match &matrix_msg.content.msgtype {
                        Some(_) => { },
                        /* report unexpectedly empty content msgtypes */
                        None => { dbg!(&matrix_msg); /* e.g. redactions */ },
                    }

                    /* TODO: consider storing a Person struct instead of a String for sender, so we can
                       keep display name and username close to the message */
                    let chat_msg : ChatMessage = ChatMessage { sender: matrix_msg.sender,
                                                               content: matrix_msg.content.to_string () /* TODO change */,
                                                               datetime,
                                                               media: vec!() }; // TODO: media

                    messages.push (chat_msg);
                },

                /* track displayname changes for users, so we can add that info to our ChatLog.people set */
                "m.room.member" => {
                    match matrix_msg.content.displayname {
                        Some(n) => { people_map.insert (matrix_msg.sender, n); },
                        None => { dbg!(matrix_msg); },
                    }
                },

                /* skip these types (for now, TODO add support for capturing some of them as ChatMessage's in the ChatLog) */
                "m.room.encrypted" | "m.room.join_rules" | "m.room.topic" | "m.room.history_visibility" | "m.room.guest_access" | "m.room.encryption" | "m.room.name" | "m.call.invite" | "m.room.avatar" | "org.matrix.msc3381.poll.start" | "org.matrix.msc3381.poll.end" => { },

                /* report unexpected message types */
                _ => { dbg!(&matrix_msg.r#type); }
            }
        }

        for (username, name) in people_map.drain () {
            let p : Person = Person { name,
                                      email: "".to_string (),
                                      username };
            people.insert (p);
        }

        return Ok(vec!(ChatLog { name: log.room_name, messages, people }));
    }
}
