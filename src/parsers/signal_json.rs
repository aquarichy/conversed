use std::fs;
use std::collections::{HashSet,HashMap};

use crate::{report_err, ChatLog, ChatMessage, ChatParser, Parse, Person, log};
use chrono::{DateTime, Utc};
use regex::Regex;

use serde::Deserialize;

/* Parses data extracted from Signal's SQLite database (database.sqlite) as JSON
 * using the 'signal_export_to_json.sh' script found in our 'scripts/'
 * directory.
 *
 * Currently, steps to make use of this are:
 * - use Signal's own backup mechanism to obtain an encrypted backup file
 * - decrypt it using one of the third-party tools available, e.g.
 *   https://github.com/mossblaser/signal_for_android_decryption.git
 *   (DISCLAIMER: I cannot offer any guarantees regarding that project, as I
 *   am not a contributor) to obtain the 'database.sqlite' in the root directory
 *   of the decrypted backup file tree.
 * - use scripts/signal_export_to_json.sh from this project to
 *   extract thread and message information in JSON format (e.g. pipe it to 'database.json')
 * - run the 'conversed' binary on the JSON file
 *
 * NOTE: at this time, two branches exist, signal-rusqlite and signal-sqlite
 * that parse the database.sqlite file directly, but issues exist trying
 * to use those parsers via WASM in the Node.js-based 'Conversed' front-end.
 * There is no work yet to operate directly on the encrypted database.
 */

struct SignalJsonParser {
}

#[derive(Debug,Deserialize)]
struct SignalJson {
    threads: Vec<SignalJsonThread>,
    messages: Vec<SignalJsonMessage>,
}

#[derive(Debug,Deserialize)]
struct SignalJsonThread {
    thread_id: i64,
    chat_name: String,
}

#[derive(Debug,Deserialize)]
struct SignalJsonMessage {
    thread_id         : i64,
    from_recipient_id : i64,
    name              : String,
    email_substitute  : String,
    username_sysid    : String,
    unixtime          : i64,
    content           : String,
}

pub fn new () -> ChatParser {
    return ChatParser { service: "signal".to_string (), format: "json".to_string (), parser: Box::new (SignalJsonParser {}) };
}

impl Parse for SignalJsonParser {
    fn can_parse (&self, file: &String) -> bool {
        let re_str : &str = "(^|/)database.json";
        let re : Regex = match Regex::new (re_str) {
            Ok(re) => re,
            Err(e) => { eprintln!("Error compiling regex: {e}. re_str: '{re_str}'"); return false },
        };

        return re.is_match (file);
    }

    fn parse (&self, file: &String) -> Result<Vec<ChatLog>,String> {
        let data : String = match fs::read_to_string (file) {
            Ok(d) => d,
            Err(e) => {
                let msg : String = format!("Uh oh, couldn't parse chat log: {e}");
                log (msg.as_str ());
                return Err (msg);
            }
        };

        return self.parse_data (data);
    }

    fn parse_data (&self, data : String) -> Result<Vec<ChatLog>,String> {
        let mut chat_logs : Vec<ChatLog> = vec!();
        let mut chat_log_map : HashMap<i64,ChatLog> = HashMap::new ();
        let mut recipient_map : HashMap<i64,Person> = HashMap::new ();

        let obj : SignalJson = match serde_json::from_str (&data) {
            Ok(o) => o,
            Err(e) => return Err(format!("{}", e)),
        };

        let thread_list : Vec<SignalJsonThread> = obj.threads;
        let messages_list : Vec<SignalJsonMessage> = obj.messages;

        for thread in thread_list {
            let chat_log : ChatLog = ChatLog { messages: vec!(),
                                               name: thread.chat_name,
                                               people: HashSet::new () };
            chat_log_map.insert (thread.thread_id, chat_log);
        };

        for signal_message in messages_list {
            let datetime : DateTime<Utc> = DateTime::from_timestamp (signal_message.unixtime / 1000, (signal_message.unixtime % 1000 * 1000000) as u32).unwrap ();
            let msg : ChatMessage = ChatMessage { sender: signal_message.name,
                                                  content: signal_message.content,
                                                  datetime, media: vec!() };

            match chat_log_map.get_mut (&signal_message.thread_id) {
                Some(c) => {
                    let person : &Person = recipient_map.entry (signal_message.from_recipient_id).or_insert_with (|| {
                        Person { name: msg.sender.clone (),
                                 username: signal_message.username_sysid,
                                 email: signal_message.email_substitute }
                    });

                    if ! c.people.contains (person) {
                        c.people.insert (person.clone ());
                    }

                    c.messages.push (msg);
                },
                None => {
                    return report_err (format!("ERROR: found message with unknown thread_id {}.  Message: {:?}", signal_message.thread_id, msg));
                }
            }
        }

        for (_id, chat_log) in chat_log_map.drain () {
            chat_logs.push (chat_log);
        }

        return Ok(chat_logs);
    }
}
