use crate::{ChatParser,ChatLog,Parse,ChatMessage,Person};

use std::str::Split;
use std::collections::HashSet;
use std::path::PathBuf;

use chrono::{DateTime, Utc};

struct CicqChatParser {
    my_name: String
}

impl Parse for CicqChatParser {
    fn can_parse (&self, dir_path_s: &String) -> bool {
        let dir_path : PathBuf = PathBuf::from (dir_path_s);
        return dir_path.join ("info").exists () && dir_path.join ("about").exists () && dir_path.join ("history").exists () && dir_path.join ("lastread").exists ();
    }

    fn parse (&self, dir_path_s : &String) -> Result<Vec<ChatLog>,String> {
        let dir_path : PathBuf = PathBuf::from (dir_path_s);
        if dir_path.join ("info").exists () && dir_path.join ("about").exists () && dir_path.join ("history").exists () && dir_path.join ("lastread").exists () {
            let person : Person = self.parse_info (dir_path.join ("info"))?;
            return self.parse_history (dir_path.join ("history"), person);
        } else {
            return Err(format!("Expected path to directory containing files 'info', 'about', 'history' and 'lastread' but '{}' did not contain those.",
                               dir_path.to_str ().unwrap ()));
        }
    }

    /* Expects the content of the 'info' and 'history' files to be concatenated, separated by a string '<FILESEP>'.
    Any instance of '<FILESEP>' as actual content of either file should be escaped by the caller in advance. */
    fn parse_data (&self, mut _data : String) -> Result<Vec<ChatLog>,String> {
        let mut parts : Split<&str> = _data.split ("<FILESEP>");

        let info : &str = match parts.next () {
            Some(info) => info,
            None => { return Err (String::from ("ERROR: failed to get 'info' contents from supplied data")); }
        };
        let history : &str = match parts.next () {
            Some(history) => history,
            None => { return Err (String::from ("ERROR: failed to get 'history' contents from supplied data")); }
        };

        let their_name : Person = self.parse_info_data (info.to_string ())?;
        return self.parse_history_data (history.to_string (), their_name);
    }
}

pub fn new () -> ChatParser {
    return ChatParser { service: "cicq".to_string (), format: "html".to_string (), parser: Box::new(CicqChatParser { my_name: "Me".to_string () }) };
}

impl CicqChatParser {
    fn parse_info_data (&self, info_str : String) -> Result<Person,String> {
        let mut info_lines : Split<&str> = info_str.split ("\n");
        let username : String = info_lines.nth (0).unwrap ().to_string ();
        let account_email : String = info_lines.nth (2).unwrap ().to_string ();
        let _profile_url : String = info_lines.nth (24).unwrap ().to_string ();
        let name : String = info_lines.nth (16).unwrap ().to_string ();

        return Ok (Person {name, username, email: account_email});
    }

    fn parse_info (&self, info_path : PathBuf) -> Result<Person,String> {
        let info_str : String = match std::fs::read_to_string (&info_path) {
            Ok(s) => s,
            Err(e) => return Err (format!("Error reading from {}: {}", info_path.to_str ().unwrap (), e)),
        };

        return self.parse_info_data (info_str);
    }

    fn parse_message (&self, msg: &str, other_name: &String) -> Result<Option<ChatMessage>,String> {
        if msg == "" {
            return Ok(None);
            /* skip initial empty message before first \x0C */
        }

        let mut msg_parts : Split<&str> = msg.split ("\n");


        let first = msg_parts.next ().unwrap ();
        if first != "" {
            eprintln!("WARNING: first part wasn't empty str?!  Found '{}'", first);
        }

        let dir : &str = match msg_parts.next () {
            Some(d) => d,
            None => return Err (format!("WARNING: no content in msg '{}'?", msg)),
        };
        let field_type : &str = msg_parts.next ().unwrap ();
        let ts1 : &str = msg_parts.next ().unwrap ();
        let ts2 : &str = msg_parts.next ().unwrap ();

        if field_type != "MSG" {
            eprintln! ("WARNING: field_type '{}' != 'MSG'.  Unexpected field type.  Continuing anyway.", field_type);
        }
        if ts1 != ts2 {
            eprintln!("WARNING: ts1 {} != ts2 {}!  Unexpected difference.  Going with ts1.", ts1, ts2);
        }

        let msg_parts : Vec<&str> = msg_parts.collect ();
        let msg : String = msg_parts.join ("\n");

        /* Timestamp likely Unix time: seconds since the epoch (1970-01-01 00:00:00 UTC) */
        let dt : DateTime<Utc> = DateTime::from_timestamp (ts1.parse ().expect ("Failed to parse timestamp"), 0).unwrap ().into ();

        let sender : &String = if dir == "OUT" { &self.my_name } else { &other_name };

        /* TODO: investigate media support */
        return Ok (Some (ChatMessage { datetime: dt, sender: sender.to_string (), content: msg, media: vec!() }));
    }

    fn parse_history_data (&self, history_str : String, person : Person) -> Result<Vec<ChatLog>,String> {
        let mut chat_log : ChatLog = ChatLog { name: format!("{} ({}, {})", person.name, person.username, person.email),
                                               messages: vec!(),
                                               people: HashSet::new () };
        for msg in history_str.split ("\x0C") {
            let msg_opt = self.parse_message (msg, &person.name)?;
            match msg_opt {
                Some (msg) => chat_log.messages.push (msg),
                None => {} /* there are empty records possible :) */
            };
        }

        chat_log.people.insert (person);
        chat_log.people.insert (Person { name: self.my_name.clone (), username: String::new (), email: String::new ()  });

        return Ok(vec!(chat_log));
    }

    fn parse_history (&self, history_path : PathBuf, person : Person) -> Result<Vec<ChatLog>,String> {
        if let Ok(history_str) = std::fs::read_to_string (&history_path) {
            return self.parse_history_data (history_str, person);
        } else {
            return Err (format !("Couldn't read from history_path at '{}'", history_path.to_str ().unwrap ()));
        }
    }
}
