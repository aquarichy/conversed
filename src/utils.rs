use console_error_panic_hook;

pub fn set_panic_hook() {
    // For more details see:
    // https://github.com/rustwasm/console_error_panic_hook#readme
    console_error_panic_hook::set_once();
}
