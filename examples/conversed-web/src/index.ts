import * as fs from "node:fs";
import * as FbCicqParser from "../../../pkg/conversed.js";
import { Parser, ChatLog, ParseCallback } from "./conversed_types.js";
import * as mysql from "mysql";

class FbHtmlParser implements Parser {
  readonly service: string = "meta_html";
  readonly format: string = "html";

  public parse (conn : mysql.Connection, fpath: string, identity_service_cb : ParseCallback, _explore : boolean) {
    fs.readFile (fpath, (err, data) => {
      if (err) {
        console.error (`ERROR: {err}`);
      } else {
        const chat_logs : ChatLog[] = FbCicqParser.parse_chat_log_js ("meta_html", data.toString ());
        const chat_log : ChatLog = chat_logs[0];
        chat_log.messages.forEach (m => m.datetime = new Date (m.datetime)); // deserialize datetime from string to Date
        identity_service_cb (conn, chat_log, fpath, this.service);
      }
    });

  }
}

const fb_parser = new FbHtmlParser ();

fb_parser.parse ({} as mysql.Connection, "../../test_data/meta_html.2024-02.sp_rf/message_1.html", (_conn, chat_log, _fpath, _service) => {
  console.log (chat_log);
  console.log (chat_log.name);
  console.log (chat_log.messages);
  
  console.log (chat_log.messages[10].datetime);
  console.log (chat_log.messages[10].datetime.getFullYear ())
}, false);

