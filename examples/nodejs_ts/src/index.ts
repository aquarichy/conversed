import * as fs from "node:fs";
import * as rusty from "../../../pkg/conversed.js";

const test_dir : string = "../../test_data";
const meta_html_test_dir : string = `${test_dir}/meta_html.2024-02.sp_rf`;
const cicq_test_dir : string =`${test_dir}/cicq.sp_kp`;

/* fb */
let data : string = fs.readFileSync (`${meta_html_test_dir}/message_1.html`, "utf8");
let chat_logs_meta = rusty.parse_chat_log_js ("meta_html", data); /* TODO: improve WASM to actually print errors to console */
let chat_log_meta = chat_logs_meta[0];

console.log ("meta: chat_log: ", chat_log_meta);
console.log ("meta: chat_log.name: ", chat_log_meta.name);
console.log ("meta: chat_log.messages: ", chat_log_meta.messages);
console.log ("meta: chat_log.messages[10].datetime: ", chat_log_meta.messages[10].datetime);

/* cicq */
const info_data : string = fs.readFileSync (`${cicq_test_dir}/info`, "utf8");
const history_data : string = fs.readFileSync (`${cicq_test_dir}/history`, "utf8");
data = `${info_data}<FILESEP>${history_data}`;

let chat_logs_cicq = rusty.parse_chat_log_js ("cicq", data);
let chat_log_cicq = chat_logs_cicq[0];

console.log ("cicq: chat_log: ", chat_log_cicq);
console.log ("cicq: chat_log.name: ", chat_log_cicq.name);
console.log ("cicq: chat_log.messages: ", chat_log_cicq.messages);
console.log ("cicq: chat_log.messages[10].datetime: ", chat_log_cicq.messages[10].datetime);
